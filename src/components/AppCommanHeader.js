import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text,Platform} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import {pop,openSideMenu } from '../controlls/NavigationControll'
import { BASE_END_POINT} from '../AppConfig';
import {getUnreadNotificationsCount} from '../actions/NotificationAction'
import {arrabicFont,englishFont,boldFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {showMenu,hideMenu} from '../actions/MenuActions'

class AppCommanHeader extends Component {


    render(){
        const {isRTL,currentUser,title} = this.props;
     
        return(
            <Animatable.View animation="slideInDown" delay={300} style={{borderBottomLeftRadius:!isRTL?moderateScale(30):0,borderBottomRightRadius:isRTL?moderateScale(30):0,  backgroundColor:'white',width:wp(100)}}>
                {Platform.OS=='ios'&&<View style={{width:wp(100),height:20,backgroundColor:'white'}} />}
                
                <View style={{ alignSelf:'center',width:wp(94),marginTop:moderateScale(0),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between'}}>

                    <TouchableOpacity onPress={()=>{pop()}} style={{}}>
                        <Icon name={isRTL?'arrowright':'arrowleft'} type='AntDesign' style={{fontSize:responsiveFontSize(20),color:'black'}} />
                    </TouchableOpacity>

                    <View>
                    <FastImage
                    resizeMode='contain'
                    source={require('../assets/imgs/appLogo.png')}
                    style={{width:wp(32),height:responsiveHeight(14)}}
                    />
                    </View>

                </View>

                <Text style={{marginHorizontal:moderateScale(10),marginBottom:moderateScale(10), fontSize:responsiveFontSize(10),  color:"black", fontFamily:boldFont }}>{title}</Text>

                
            </Animatable.View>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsCount,
    showMenu,
    hideMenu
}


export default connect(mapStateToProps,mapDispatchToProps)(AppCommanHeader);
