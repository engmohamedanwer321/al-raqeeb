import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {getUnreadNotificationsCount} from '../actions/NotificationAction'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'


class NotificationCard extends Component {
    state={
        isRead:this.props.data.read,
    }

    readNotification = () =>{
        axios.put(`${BASE_END_POINT}notif/${this.props.data.id}/read`,null,{
            headers:{
                'Authorization':`Bearer ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            this.setState({isRead:true})
            this.props.getUnreadNotificationsCount(this.props.currentUser.token)

            //Alert.alert("READ")
        })
        .catch(error=>{
            //Alert.alert("ERROR   ",error)
        })
    }

    render(){
        const {isRTL,data,currentUser} = this.props;
        const {isRead} = this.state
        moment.locale(this.props.isRTL ? 'ar' : 'en');
        return(
            <TouchableOpacity onPress={()=>{this.readNotification()}}> 
            <Animatable.View 
                 style={{backgroundColor:isRead?'white':'#f7f5f5', borderBottomWidth:1,borderBottomColor:'#cccbcb', marginTop:moderateScale(0), alignSelf:'center', width:responsiveWidth(100),paddingHorizontal:moderateScale(5),borderTopLeftRadius:moderateScale(8),borderTopRightRadius:moderateScale(8)}}
                 animation={"flipInX"} 
                 duration={1000}>
                    <View style={{}}>   
                    <View style={{marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',width:responsiveWidth(94)}}>
                        <Thumbnail large source={currentUser.user.type=='PURCHASING'?data.restaurant.img?{uri:data.restaurant.img}:require('../assets/imgs/profileicon.jpg'):data.order.restaurant.img?{uri:data.order.restaurant.img}:require('../assets/imgs/profileicon.jpg')} />   
                        <View style={{width:responsiveWidth(70), marginHorizontal:moderateScale(3)}}>
                            <Text style={{alignSelf:isRTL?'flex-start':'flex-end', fontSize:responsiveFontSize(5), marginTop:moderateScale(3), fontFamily:isRTL?arrabicFont:englishFont,color:'gray'}}>{moment(data.createdAt).fromNow()}</Text>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start',color:colors.darkBlue, fontSize:responsiveFontSize(7), fontFamily:isRTL?arrabicFont:englishFont}}>{currentUser.user.type=='PURCHASING'?data.restaurant.restaurantName: data.order.restaurant.restaurantName}</Text>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color:'gray',fontSize:responsiveFontSize(5), fontFamily:isRTL?arrabicFont:englishFont}}>{currentUser.user.type=='PURCHASING'?data.restaurant.phone:data.order.restaurant.phone}</Text>
                        </View>
                    </View>
                    <View style={{width:responsiveWidth(80),alignSelf:'center',marginBottom:moderateScale(6)}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color:colors.black, fontSize:responsiveFontSize(6), fontFamily:isRTL?arrabicFont:englishFont}}>{isRTL?data.arabicDescription:data.description} </Text>
                    </View>
                </View>      
                </Animatable.View>
        </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsCount
}


export default connect(mapStateToProps,mapDispatchToProps)(NotificationCard);
