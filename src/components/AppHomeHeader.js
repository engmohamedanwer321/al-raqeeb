import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text,Platform} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import {pop,openSideMenu,resetTo } from '../controlls/NavigationControll'
import { BASE_END_POINT} from '../AppConfig';
import {getUnreadNotificationsCount} from '../actions/NotificationAction'
import {arrabicFont,englishFont,boldFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {showMenu,hideMenu} from '../actions/MenuActions'

class AppHomeHeader extends Component {


    render(){
        const {isRTL,currentUser,home,hideBack} = this.props;
     
        return(
            <Animatable.View animation="slideInDown" delay={300} style={{borderBottomLeftRadius:!isRTL?moderateScale(30):0,borderBottomRightRadius:isRTL?moderateScale(30):0,  backgroundColor:'white',width:wp(100)}}>
                {Platform.OS=='ios'&&<View style={{width:wp(100),height:20,backgroundColor:'white'}} />}
                
                <View style={{alignSelf:'center',width:wp(94),marginTop:moderateScale(0),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between'}}>

                    {hideBack?
                    <View style={{width:wp(10)}} />
                    :
                    <TouchableOpacity onPress={()=>{
                        if(home){
                            resetTo('Home')
                        }else{
                            pop()
                        }
                    }} style={{}}>
                        <Icon name={isRTL?'arrowright':'arrowleft'} type='AntDesign' style={{fontSize:responsiveFontSize(20),color:'black'}} />
                    </TouchableOpacity>
                    }

                    <FastImage
                    resizeMode='contain'
                    source={require('../assets/imgs/appLogo.png')}
                    style={{width:wp(32),height:responsiveHeight(14)}}
                    />

                </View>

                <View style={{marginBottom:moderateScale(10), alignSelf:'center',width:wp(94)}}>
                    <View style={{flexDirection:isRTL?'row-reverse':'row'}}>
                        <View>
                            <FastImage
                            source={require('../assets/imgs/profileicon.jpg')}
                            style={{width:70,height:70,borderRadius:35,borderWidth:3,borderColor:colors.darkBlue}}
                            />
                             <View style={{justifyContent:'center',alignItems:'center', marginTop:moderateScale(-12), alignSelf:isRTL?'flex-start':'flex-end',width:25,height:25,borderRadius:12.5,backgroundColor:colors.darkBlue}}>
                                <Icon name={'star'} type='AntDesign' style={{fontSize:responsiveFontSize(7),color:'white'}} />
                            </View>
                        </View>
                        <View style={{marginHorizontal:moderateScale(2),marginTop:moderateScale(4)}}>
                            <Text style={{width:wp(70), alignSelf:isRTL?'flex-end':'flex-start', fontSize:responsiveFontSize(8),  color:"black", fontFamily:boldFont,textAlign:isRTL?'right':'left' }}>{currentUser?currentUser.first_name+" "+currentUser.last_name:''}</Text>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start', fontSize:responsiveFontSize(7),  color:colors.darkGray, fontFamily:isRTL?arrabicFont:englishFont }}></Text>
                        </View>
                    </View>

                    <TouchableOpacity
                     onPress={()=>{
                        this.props.showMenu()
                     }}
                     style={{marginTop:moderateScale(-5), alignSelf:isRTL?'flex-start':'flex-end', }}>
                        <FastImage
                        resizeMode='contain'
                        source={require('../assets/imgs/burgerIcon.png')}
                        style={{ width:wp(15),height:hp(5),}}
                        />
                    </TouchableOpacity>
                </View>

            </Animatable.View>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsCount,
    showMenu,
    hideMenu
}


export default connect(mapStateToProps,mapDispatchToProps)(AppHomeHeader);
