import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT} from '../AppConfig';
import {
    UNREAD_NOTIFICATIONS_COUNT
} from './types';
import { RNToasty } from 'react-native-toasty';

export function getUnreadNotificationsCount(token) {
    return dispatch => {
        axios.get(`${BASE_END_POINT}notif/unreadCount`, {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`  
            },
        })
        .then(response=>{
            console.log('noti count 22')
            console.log(response.data);
            dispatch({type:UNREAD_NOTIFICATIONS_COUNT,payload:response.data.unread})
        }).catch(error=>{
            console.log('noti count error 22')
            console.log(error.response);
        })
    }
}



