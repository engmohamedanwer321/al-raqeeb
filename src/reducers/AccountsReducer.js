import * as types from "../actions/types"

const initialState = {
    currentUser: null,
    loading: false,
    errorText: null,
    userType: "CLIENT",
    userToken: null,
    currentLocation: [0, 0],
}

const AccountsReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.ACCOUNTS_COUNT:
            return { ...state, accountsCount: action.payload }

        case types.LOGOUT:
            return { ...state, currentUser: null }
        default:
            return state;
    }

}

export default AccountsReducer;