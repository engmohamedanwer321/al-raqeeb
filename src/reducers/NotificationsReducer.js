import {
    UNREAD_NOTIFICATIONS_COUNT
} from '../actions/types';

const initState = {
    unreadNotificationsCount:0,
}

const NotificationsReducer = (state=initState, action) => {
    switch(action.type){
        case UNREAD_NOTIFICATIONS_COUNT:
            console.log("nossss "+ action.payload)
            return { ...state,unreadNotificationsCount:action.payload };        
        default: 
            return state; 
    }
}

export default NotificationsReducer;