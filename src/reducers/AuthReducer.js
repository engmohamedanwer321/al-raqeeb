import * as types from "../actions/types"

const initialState = {
    currentUser: null,
    loading: false,
    errorText: null,
    userType:"CLIENT",
    userToken: null,
    currentLocation: [0,0],
}

const AuthReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.CURRENT_USER:
            return { ...state, currentUser: action.payload }
            
        case types.LOGOUT:
            return{...state,currentUser:null}    

        /*case types.USER_LOCATION:
             return { ...state, currentLocation:action.payload }
       

        case types.CHECK_USER_TYPE:
            return {...state, userType : action.payload }
        case types.USER_TOKEN:
            return {...state,userToken:action.payload}    
        case types.LOGOUT:
             return {...state,currentUser:null}*/

        default:
            return state;
    }

}

export default AuthReducer;