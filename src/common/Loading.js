//
import React, { Component } from "react";
import { Text, View,ActivityIndicator} from "react-native";
import LottieView from 'lottie-react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";


class Loading extends Component {
render(){
    return(
        <View style={{width:responsiveWidth(25), marginTop:moderateScale(20), backgroundColor:'tranparent',justifyContent:'center',alignItems:'center',alignSelf:'center'}}>
            <LottieView
            style={{width:responsiveWidth(50)}}
            source={require('../assets/animations/bigSpinner.json')}
            autoPlay
            loop
            />
           
        </View>
    )
}
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(Loading);
