//
import React, { Component } from "react";
import { Text, View} from "react-native";
import LottieView from 'lottie-react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";


class Spiner extends Component {
render(){
    return(
        <View style={{alignSelf:'center',marginTop:moderateScale(10), backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
            <LottieView
            style={{width:responsiveWidth(50)}}
            source={require('../assets/animations/bigSpinner.json')}
            autoPlay
            loop
            />
        </View>
    )
}
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(Spiner);
