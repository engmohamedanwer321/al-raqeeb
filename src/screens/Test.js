import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView,
  TextInput,
  Alert,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';


class Test extends Component {
  
 

  render() {
    const {isRTL} = this.props;
    return (
      <View style={{justifyContent:'center',alignItems:'center',flex: 1}}>
          <Text>TEST</Text>
      </View>
    );
  }
}
const mapDispatchToProps = {
 
};

const mapToStateProps = state => ({
  isRTL: state.lang.RTL,
  showMenuModal: state.menu.showMenuModal,
  //userToken: state.auth.userToken,
});

export default connect(
  mapToStateProps,
  mapDispatchToProps,
)(Test);
