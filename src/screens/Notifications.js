import React, { Component } from 'react';
import { View, RefreshControl, ScrollView, FlatList, Alert, Text, TouchableOpacity } from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import Strings from '../assets/strings';

import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import { Icon, Thumbnail, Button } from 'native-base'
import { selectMenu, removeItem } from '../actions/MenuActions';
import { enableSideMenu, pop } from '../controlls/NavigationControll'
import NetInfo from "@react-native-community/netinfo";
import Loading from "../common/Loading"
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'
import * as colors from '../assets/colors'
import { arrabicFont, englishFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import NotificationCard from '../components/NotificationCard'


class Notifications extends Component {

    page = 1;
    state = {
        networkError: null,
        notifications: [],
        notificationsRefresh: false,
        notificationsLoading: true,
        notifications404: false,
        pages: null,
    }



    componentDidMount() {
        enableSideMenu(false, null)
        /*NetInfo.addEventListener(state => {
            if(state.isConnected){
                this.page=1
                this.getRooms(1,false);
                this.setState({loading:true,networkError:false})
            }else{
                this.setState({loading:false,networkError:true})
            }
          }); */
        this.getNotifications(false, 1)
    }

    getNotifications(refresh, page) {
        if (refresh) {
            this.setState({ notificationsRefresh: true })
        }
        axios.get(`${BASE_END_POINT}notif?page=${page}`, {
            headers: {
                'Authorization': `Bearer ${this.props.currentUser.token}`
            }
        })
            .then(response => {
                console.log('Done   ', response.data.data)
                //Alert.alert("ddd")
                this.setState({
                    notifications: refresh ? response.data.data : [...this.state.notifications, ...response.data.data],
                    notificationsLoading: false,
                    notificationsRefresh: false,
                    notifications404: false,
                    pages: response.data.pageCount,
                })
            })
            .catch(error => {
                console.log('Error   ', error)
                this.setState({ notifications404: true, notificationsLoading: false, })
            })
    }

    notificationsPage = () => {
        const { notificationsRefresh, notifications, notifications404, notificationsLoading, pages } = this.state
        return (
            notifications404 ?
                <NetworError />
                :
                notificationsLoading ?
                    <Loading />
                    :
                    notifications.length > 0 ?
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginTop: moderateScale(10) }}
                            data={notifications}
                            renderItem={({ item }) => <NotificationCard data={item} />}
                            onEndReachedThreshold={.5}
                            onEndReached={() => {
                                if (this.page <= pages) {
                                    this.page = this.page + 1;
                                    this.getNotifications(false, this.page)
                                    console.log('page  ', this.page)
                                }
                            }}
                            refreshControl={
                                <RefreshControl
                                    //colors={["#B7ED03",colors.darkBlue]} 
                                    refreshing={notificationsRefresh}
                                    onRefresh={() => {
                                        this.page = 1
                                        this.getNotifications(true, 1)
                                    }}
                                />
                            }

                        />
                        :
                        <NoData />
        )
    }

    componentWillUnmount() {
        this.props.removeItem()
    }


    render() {
        const { isRTL } = this.props;
        return (
            <View style={{ flex: 1 }} >

                <View style={{ flex: 1, height: responsiveHeight(73), backgroundColor: colors.white, borderTopRightRadius: moderateScale(20), borderTopLeftRadius: moderateScale(20), marginTop: moderateScale(15), width: responsiveWidth(100), marginBottom: responsiveHeight(10) }} >
                    {this.notificationsPage()}
                </View>

            </View>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    barColor: state.lang.color,
    currentUser: state.auth.currentUser,
})

const mapDispatchToProps = {
    removeItem,
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
