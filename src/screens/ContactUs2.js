import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppCommanHeader from '../components/AppCommanHeader'


class Register extends Component {

    state = {
        subject:' ',
        message:' ',

        firstName:' ',
        nationality:' ',
        email: ' ',
        password: ' ',
        id:' ',
        birthDate:' ',
        loading: false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

  

    login = () => {
        const { password, email } = this.state


        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '' })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }
        if (email.replace(/\s/g, '').length && password.replace(/\s/g, '').length) {
            this.setState({ loading: true })
            const data = {
                username: userName,
                password: password,
                token: '',
            }
            axios.post(`${BASE_END_POINT}signin`, JSON.stringify(data), {
                headers: {
                    "Content-Type": 'application/json'
                }
            })
                .then(response => {
                    this.setState({ loading: false })
                    //console.log(response.data)
                    this.props.setUser(response.data)
                    AsyncStorage.setItem('USER', JSON.stringify(response.data))
                    if (response.data.user.type == 'USER') {
                        resetTo('ResturantHome')
                    } else if (response.data.user.type == 'SURVEY') {
                        resetTo('SurveyHome')
                    } else if (response.data.user.type == 'ADMIN') {
                        resetTo('AdminHome')
                    } else if (response.data.user.type == 'DRIVER') {
                        resetTo('DriverHome')
                    } else if (response.data.user.type == 'OPERATION') {
                        resetTo('OperationHome')
                    }
                    else if (response.data.user.type == 'PURCHASING') {
                        resetTo('PurchasingHome')
                    }
                })
                .catch(error => {
                    this.setState({ loading: false })
                    if (error.response.status == 401) {
                        RNToasty.Error({ title: strings.loginUserDataIncorrect })
                    }
                })
        }

    }

    sendButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => {push('Home') /*this.login()*/ }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginTop: moderateScale(15), height: responsiveHeight(8), width: wp(80), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(5) }} >
                <Text style={{fontSize:responsiveFontSize(8), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.send}</Text>
            </TouchableOpacity>
        )
    }

    messageInput = () => {
        const {isRTL} = this.props
        const {message} = this.state
        return(
            <View style={{marginTop:moderateScale(10),width:responsiveWidth(80), alignSelf:'center'}}>             
                <Text style={{marginBottom:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont, color:colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}}> {Strings.message}</Text>
                <View style={{borderRadius:moderateScale(6), backgroundColor:'white' }} >
                <TextInput 
                    multiline
                     onChangeText={(val)=>{this.setState({message:val})}}
                     style={{textAlignVertical:'top', width:responsiveWidth(80), paddingHorizontal:moderateScale(3),fontFamily:isRTL?arrabicFont:englishFont,height:responsiveHeight(25)}}
                     //placeholder={Strings.message}
                />
                </View>
                {message.length==0&&
                <Text style={{color:'red',marginHorizontal:moderateScale(2),fontSize:responsiveFontSize(6),alignSelf:isRTL?'flex-start':'flex-end'}}> {Strings.require}</Text>
                }
            </View>
        )
    }

    subjectInput = () => {
        const { isRTL } = this.props
        const { subject } = this.state
        return (
            <View style={{marginTop: moderateScale(15), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.subject}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ subject: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.subject}
                    />
                </Item>

                {subject.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


  


    render() {
        const { loading } = this.state
        return (
            <View style={{backgroundColor:colors.grayBackground,flex:1}}>
                    <AppCommanHeader title={Strings.contactUs} />
                    <ScrollView showsVerticalScrollIndicator={false}>
                    {this.subjectInput()}
                    {this.messageInput()}
                    {this.sendButton()}
                    </ScrollView>
                
                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }

            

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(Register);

