import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView,Linking, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppCommanHeader from '../components/AppCommanHeader'
import Menu from '../components/Menu'

class FunctionalGynaecologyClinic extends Component {

  

    componentDidMount() {
        enableSideMenu(false, null)
    }



    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                
                <AppCommanHeader title={Strings.functionalGynaecologyClinc} />
          
                <ScrollView showsVerticalScrollIndicator={false}>
                   
                    <View style={{minHeight:responsiveHeight(80),paddingBottom:moderateScale(20), backgroundColor:"#946AB3",width:wp(90),borderRadius:moderateScale(6),alignSelf:'center',marginTop:moderateScale(5),marginBottom:moderateScale(20)}} >
                        
                    <View style={{alignSelf:'center',marginTop:moderateScale(10)}}>
                    
                    <FastImage
                        resizeMode='contain'
                        source={require('../assets/imgs/drAmrabdelYazed.png')}
                        style={{width:wp(80),height:responsiveHeight(35),borderRadius:moderateScale(6)}}
                    />


                    <View style={{alignSelf:'center',width:wp(80),marginTop:moderateScale(2),}}>                            
                        <Text style={{alignSelf:'flex-start', fontSize:responsiveFontSize(7), color: colors.white, fontFamily:englishFont }}>Consultant Dr. Amr Abo El Yazid</Text>
                    </View>

                    
                    </View>




                  

                   
                    </View>

                </ScrollView>



                {/*this.props.showMenuModal&&<Menu/>*/}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(FunctionalGynaecologyClinic);

