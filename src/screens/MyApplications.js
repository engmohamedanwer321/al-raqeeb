import React, { Component } from 'react';
import {
    View,FlatList,RefreshControl, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import ApllicationCard from '../components/ApllicationCard'
import Loading from '../common/Loading'
import NetworError from '../common/NetworError'
import NoData from '../common/NoData'


class MyApplications extends Component {

    page = 1;
    state = {
        networkError: null,
        applications:[],
        applicationsLoading: true,
        applicationsRefresh:false,
        applications404:false,
        pages: null,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.getApplications(false)
    }

    getApplications = (refresh) => {
        if(refresh){
            this.setState({applicationsRefresh:true})
        }
        axios.get(`${BASE_END_POINT}applications/users/${this.props.currentUser.id}`,{
            headers:{
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
          console.log('Done   ',response.data.data)
          this.setState({
              applications:refresh?response.data.data:[...this.state.applications,...response.data.data],
              applicationsLoading:false,
              applicationsRefresh:false,
              //pages:response.data.pageCount,
            })
          })
        .catch(error=>{
          console.log('Error   ',error.response)
          this.setState({applications404:true,applicationsLoading:false,})
        })
    }

    applicationsList = () => {
        const {isRTL} = this.props
        const {applications,applications404,applicationsLoading,applicationsRefresh} = this.state
        return(
            applications404?
            <NetworError />
            :
            applicationsLoading?
            <Loading />
            :
            applications.length>0?
            <FlatList 
            showsVerticalScrollIndicator={false}
            style={{backgroundColor:'white',marginTop:moderateScale(6)}}
            contentContainerStyle={{paddingBottom:moderateScale(5)}}
            data={applications}
            renderItem={({item})=><ApllicationCard data={item}/>}
            onEndReachedThreshold={.5}       
            refreshControl={
                <RefreshControl 
                    refreshing={applicationsRefresh}
                    onRefresh={() => {
                        this.page = 1
                        this.getApplications(true)
                    }}
                />
            }              
            />
            :
            <NoData />
        )
    }


    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader />
                {this.applicationsList()}
                {this.props.showMenuModal&&<Menu/>}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser: state.auth.currentUser,

    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(MyApplications);

