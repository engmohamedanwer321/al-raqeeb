import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView,
  Linking,
  TextInput,
  Alert,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Button, Item, Input, Label} from 'native-base';
import {setUser} from '../actions/AuthActions';
import {
  responsiveHeight,
  responsiveWidth,
  moderateScale,
  responsiveFontSize,
} from '../utils/responsiveDimensions';
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {
  enableSideMenu,
  resetTo,
  push,
  pop,
} from '../controlls/NavigationControll';
import {arrabicFont, englishFont, boldFont} from '../common/AppFont';
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import {removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image';
import {RNToasty} from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import strings from '../assets/strings';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import AppCommanHeader from '../components/AppCommanHeader';
import Menu from '../components/Menu';

class OncolplasticBreastReconstructionClinc extends Component {
  componentDidMount() {
    enableSideMenu(false, null);
  }

  registerButton = () => {
    const {isRTL} = this.props;

    return (
      <TouchableOpacity
        onPress={() => {
          /*this.login()*/
        }}
        style={{
          flexDirection: isRTL ? 'row-reverse' : 'row',
          alignSelf: 'center',
          marginTop: moderateScale(15),
          height: responsiveHeight(7),
          width: wp(80),
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.white,
          borderRadius: moderateScale(5),
        }}>
        <Icon
          name="square-edit-outline"
          type="MaterialCommunityIcons"
          style={{color: colors.lightBlue, fontSize: responsiveFontSize(8)}}
        />
        <Text
          style={{
            fontSize: responsiveFontSize(8),
            marginHorizontal: moderateScale(2),
            color: colors.lightBlue,
            fontFamily: isRTL ? arrabicFont : englishFont,
          }}>
          {Strings.save}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {isRTL} = this.props;
    return (
      <View style={{backgroundColor: colors.lightGray, flex: 1}}>
        <AppCommanHeader
          title={Strings.OncolplasticBreastReconstructionClinc}
        />

        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              paddingBottom: moderateScale(20),
              backgroundColor: '#946AB3',
              width: wp(90),
              borderRadius: moderateScale(6),
              alignSelf: 'center',
              marginTop: moderateScale(5),
              marginBottom: moderateScale(20),
            }}>
            <View
              style={{
                alignSelf: 'center',
                width: wp(80),
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: moderateScale(10),
              }}>
              <View style={{}}>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(9),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Prof
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(9),
                    marginTop: moderateScale(0),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Omar Sherif Omar
                </Text>

                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  European Board Certified Breast Surgeon
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Consultant Oncoplastic Breast Surgeon
                </Text>
              </View>

              <TouchableOpacity>
                <FastImage
                  resizeMode="contain"
                  source={require('../assets/imgs/profOmarShreif.jpg')}
                  style={{
                    flex: 1,
                    width: wp(38),
                    borderRadius: moderateScale(5),
                  }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                alignSelf: 'center',
                width: wp(80),
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: moderateScale(20),
              }}>
              <View>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(9),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Prof
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(9),
                    marginTop: moderateScale(0),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Ayman Noman
                </Text>

                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  European Board Certified Plastic Surgeon
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Consultant Plastic & Reconstruction Surgeon
                </Text>
              </View>

              <TouchableOpacity>
                <FastImage
                  resizeMode="contain"
                  source={require('../assets/imgs/profAyman.jpg')}
                  style={{
                    flex: 1,
                    width: wp(38),
                    borderRadius: moderateScale(5),
                  }}
                />
              </TouchableOpacity>
            </View>

            <View style={{width: wp(80), alignSelf: 'center'}}>
              <Text
                style={{
                  alignSelf: 'flex-start',
                  fontSize: responsiveFontSize(9),
                  marginTop: moderateScale(15),
                  color: colors.white,
                  fontFamily: boldFont,
                }}>
                Specialists Oncoplastic breast Surgeons
              </Text>
            </View>

            <View
              style={{
                alignSelf: 'center',
                width: wp(80),
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: moderateScale(10),
              }}>
              <View>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(9),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Dr Ali El Sakkaf
                </Text>

                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  European Board Certified Breast Surgeon
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Specialist Oncoplastic Breast Surgeon
                </Text>
              </View>

              <TouchableOpacity>
                <FastImage
                  source={require('../assets/imgs/drAlisakkaf.jpg')}
                  style={{
                    flex: 1,
                    width: wp(38),
                    borderRadius: moderateScale(5),
                  }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                alignSelf: 'center',
                width: wp(80),
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: moderateScale(20),
              }}>
              <View>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(8),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Dr Abdelrahman Lotfy
                </Text>

                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  European Board Certified Breast Surgeon
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Specialist Oncoplastic Breast Surgeon
                </Text>
              </View>

              <TouchableOpacity>
                <FastImage
                  source={require('../assets/imgs/drAbdelrhamLotfy.jpg')}
                  style={{
                    flex: 1,
                    width: wp(38),
                    borderRadius: moderateScale(5),
                  }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                alignSelf: 'center',
                width: wp(80),
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: moderateScale(20),
              }}>
              <View>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(9),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Dr Yomna Galaa
                </Text>

                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  European Board Certified Breast Surgeon
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(8),
                    color: colors.white,
                    fontFamily: boldFont,
                  }}>
                  Specialist Oncoplastic Breast Surgeon
                </Text>
              </View>

              <TouchableOpacity>
                <FastImage
                  source={require('../assets/imgs/drYomna.jpg')}
                  style={{
                    flex: 1,
                    width: wp(38),
                    borderRadius: moderateScale(5),
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        {/*this.props.showMenuModal&&<Menu/>*/}
      </View>
    );
  }
}
const mapDispatchToProps = {
  setUser,
  removeItem,
};

const mapToStateProps = state => ({
  isRTL: state.lang.RTL,
  showMenuModal: state.menu.showMenuModal,
  currentUser: state.auth.currentUser,
  //userToken: state.auth.userToken,
});

export default connect(
  mapToStateProps,
  mapDispatchToProps,
)(OncolplasticBreastReconstructionClinc);
