import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, FlatList, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import RBSheet from "react-native-raw-bottom-sheet";
//import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";

import {countries} from '../assets/dumyData' 
import {isEmail} from '../controlls/Validations'
import moment from 'moment'
import Dialog, {DialogTitle, DialogContent,SlideAnimation } from 'react-native-popup-dialog';


nationalities = ["egypt","ksa","oman","usa",'barazil','bolanda','engeland']

class Register extends Component {

    state = {
        firstName:' ',
        lastName:' ',
        gender:null,
        phone:' ',
        nationality:null,
        countries:[],
        email: ' ',
        emailError:null,
        password: ' ',
        id:' ',
        ssn:' ',
        showCalender:false,
        date:new Date(),
        birthDate:null,

        loading: false,
        showDialog:false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        moment.locale('en')
        const result = Object.keys(countries).map(i => countries[i])
        this.setState({countries:result.sort()})
    }

    title = () => {
        const { isRTL } = this.props
        return (
            <Text style={{alignSelf:'center',marginTop:moderateScale(5), fontSize:responsiveFontSize(24),  color: 'black', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.register}</Text>
        )
    }

    backButton = () => {
        const { isRTL } = this.props
        return (
            <TouchableOpacity
            onPress={()=>{pop()}}
             style={{alignSelf:isRTL?'flex-end':'flex-start', marginTop:moderateScale(13),marginHorizontal:moderateScale(10)}}>
                <Icon name={isRTL?'arrowright':'arrowleft'} type='AntDesign' style={{fontSize:responsiveFontSize(20),color:'black'}} />
            </TouchableOpacity>
        )
    }


    firstNameInput = () => {
        const { isRTL } = this.props
        const { firstName } = this.state
        return (
            <View style={{marginTop: moderateScale(15), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.firstName}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ firstName: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.firstName}
                    />
                </Item>

                {firstName.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    lastNameInput = () => {
        const { isRTL } = this.props
        const { lastName } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.lastName}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ lastName: val }) }}
                        style={{color:'black', fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.lastName}
                    />
                </Item>

                {lastName.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    phoneInput = () => {
        const { isRTL } = this.props
        const { phone } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.phone}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ phone: val }) }}
                        style={{color:'black', fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.phone}
                    />
                </Item>

                {phone.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    nationnalityInput = () => {
        const { isRTL } = this.props
        const { nationality } = this.state
        return (
            <View style={{marginTop: moderateScale(6), width: wp(80), alignSelf: 'center', }}>
                 {nationality&&
                    <Text style={{fontSize:15, color:colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.nationality}</Text>
                 }
                <TouchableOpacity 
                onPress={()=>{
                    this.RBSheet.open()
                }}
                style={{ width:wp(80),height:responsiveHeight(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5,justifyContent:'flex-end'}} >
                    <Text style={{fontSize:nationality?responsiveFontSize(5):17, color:nationality?'black':colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{nationality?nationality:Strings.nationality}</Text>
                </TouchableOpacity>
                {nationality==false&&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    genderInput = () => {
        const { isRTL } = this.props
        const { gender } = this.state
        return (
            <View style={{marginTop: moderateScale(6), width: wp(80), alignSelf: 'center', }}>
                {gender&&
                    <Text style={{fontSize:15, color:colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.gender}</Text>
                 }
                <TouchableOpacity 
                onPress={()=>{
                    this.genderSheetRef.open()
                }}
                style={{ width:wp(80),height:responsiveHeight(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5,justifyContent:'flex-end'}} >
                    <Text style={{fontSize:gender?responsiveFontSize(5):17, color:gender?'black':colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{gender?gender:Strings.gender}</Text>
                </TouchableOpacity>
                {gender==false&&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    birthDateInput = () => {
        const { isRTL } = this.props
        const { birthDate } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                 {birthDate&&
                    <Text style={{fontSize:15, color:colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.birthDate}</Text>
                 }
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showCalender:true})
                }}
                style={{ width:wp(80),height:responsiveHeight(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5,justifyContent:'flex-end'}} >
                    <Text style={{fontSize:birthDate?responsiveFontSize(5):17,color:birthDate?'black':colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{birthDate?birthDate:Strings.birthDate}</Text>
                </TouchableOpacity>
                {birthDate==false&&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    idInput = () => {
        const { isRTL } = this.props
        const { id } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.id}</Label>    
                    <Input
                        keyboardType='phone-pad'
                        onChangeText={(val) => { this.setState({ id: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.id}
                    />
                </Item>

                {id.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    emailInput = () => {
        const { isRTL } = this.props
        const { email,emailError } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.email}</Label>    
                    <Input
                        keyboardType='email-address'
                        onChangeText={(val) => { 
                            this.setState({ email: val })
                            if(val){
                               this.setState({emailError:null}) 
                            }else{
                                this.setState({emailError:Strings.require}) 
                            }
                            /*if(!isEmail(val)){
                                this.setState({emailError:Strings.emailFormatInvalid})
                            }*/
                         }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.email}
                    />
                </Item>

                {emailError&&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {emailError}</Text>
                }
            </View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
               <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(80)}}>

                    <Label style={{color:colors.darkGray}} >{Strings.password}</Label>    
                    <Input
                        secureTextEntry
                        onChangeText={(val) => { this.setState({ password: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.password}
                    />
                </Item>
               
               
                {password.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    ssnInput = () => {
        const { isRTL } = this.props
        const { ssn,nationality } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
               <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(80)}}>

                    <Label style={{color:colors.darkGray}} >{nationality=='Egypt'?Strings.nationalityId:Strings.passportId}</Label>    
                    <Input
                        
                        onChangeText={(val) => { this.setState({ ssn: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={nationality=='Egypt'?Strings.nationalityId:Strings.passportId}
                    />
                </Item>
               
               
                {ssn.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    register = () => {
        const {firstName,lastName,phone,gender,nationality,ssn, birthDate,password,email } = this.state
        if (!firstName.replace(/\s/g, '').length) {
            this.setState({ firstName: '' })
        }
        if (!ssn.replace(/\s/g, '').length) {
            this.setState({ ssn: '' })
        }
        if (!lastName.replace(/\s/g, '').length) {
            this.setState({ lastName: '' })
        }
        if(!isEmail(email)){
            this.setState({emailError:Strings.emailFormatInvalid})
        }
        if (!email.replace(/\s/g, '').length) {
            this.setState({emailError:Strings.require})
        }  
        if (!phone.replace(/\s/g, '').length) {
            this.setState({ phone: '' })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }
        if (!gender) {
            this.setState({ gender:false })
        }
        if (!nationality) {
            this.setState({ nationality:false })
        }
        if (!birthDate) {
            this.setState({ birthDate:false })
        }

        if(nationality=='Egypt'){
            if(ssn.length!=14){
                RNToasty.Error({title:Strings.nationalityMustBe14Digits})
            }else{
                if (
                    firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && email.replace(/\s/g, '').length && isEmail(email) &&
                    phone.replace(/\s/g, '').length && password.replace(/\s/g, '').length && gender && nationality && birthDate && ssn.replace(/\s/g, '').length 
                    ){
                        this.setState({ loading: true })
                        var data= new URLSearchParams()
                        data.append('first_name',firstName)
                        data.append('last_name',lastName)
                        data.append('email',email)
                        data.append('phone',phone)
                        data.append('nationality',nationality)
                        data.append('gender',gender)
                        data.append('ssn',ssn)
                        data.append('role','patient')
                        data.append('date_of_birth',birthDate)
                        data.append('password',password)
                        axios.post(`${BASE_END_POINT}users/`, data, {
                            headers: {
                                "Content-Type": 'application/x-www-form-urlencoded'
                            }
                        })
                        .then(response => {
                            this.setState({showDialog:true, loading: false })
                            console.log(response.data)
                            //RNToasty.Success({title:Strings.mustBeVerified,duration:1})
                            //this.props.setUser(response.data)
                            //AsyncStorage.setItem('USER', JSON.stringify(response.data))
                                
                        })
                        .catch(error => {
                            console.log("Error   ",error.response)
                            this.setState({ loading: false })
                            if (error.response.status == 500) {
                                RNToasty.Error({ title: strings.birthDateInvalid })
                            }
            
                            if (error.response.status == 409) {
                                RNToasty.Error({ title: error.response.data.err })
                            }
                        })
                    }
            }
        }else{
            if (
                firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && email.replace(/\s/g, '').length && isEmail(email) &&
                phone.replace(/\s/g, '').length && password.replace(/\s/g, '').length && gender && nationality && birthDate && ssn.replace(/\s/g, '').length 
                ){
                    this.setState({ loading: true })
                    var data= new URLSearchParams()
                    data.append('first_name',firstName)
                    data.append('last_name',lastName)
                    data.append('email',email)
                    data.append('phone',phone)
                    data.append('nationality',nationality)
                    data.append('gender',gender)
                    data.append('ssn',ssn)
                    data.append('role','patient')
                    data.append('date_of_birth',birthDate)
                    data.append('password',password)
                    axios.post(`${BASE_END_POINT}users/`, data, {
                        headers: {
                            "Content-Type": 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(response => {
                        this.setState({showDialog:true, loading: false })
                        console.log(response.data)
                        //RNToasty.Success({title:Strings.mustBeVerified,duration:1})
                        //this.props.setUser(response.data)
                        //AsyncStorage.setItem('USER', JSON.stringify(response.data))
                            
                    })
                    .catch(error => {
                        console.log("Error   ",error.response)
                        this.setState({ loading: false })
                        if (error.response.status == 500) {
                            RNToasty.Error({ title: strings.birthDateInvalid })
                        }
        
                        if (error.response.status == 409) {
                            RNToasty.Error({ title: error.response.data.err })
                        }
                    })
                }
        }

    }

    registerButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => {
                this.register()
             }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginVertical: moderateScale(15), height: responsiveHeight(7), width: wp(80), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(5) }} >
                <Icon name='square-edit-outline' type='MaterialCommunityIcons' style={{color:'white',fontSize:responsiveFontSize(15)}} />
                <Text style={{fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.register}</Text>
            </TouchableOpacity>
        )
    }


  
    nationalitySheet = () =>{
        const { isRTL } = this.props
        const { email } = this.state
        return (
            <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            height={responsiveHeight(40)}
            duration={250}
            customStyles={{
              container: {
                justifyContent: "center",
                alignItems: "center",
                borderTopLeftRadius:moderateScale(5),
                borderTopRightRadius:moderateScale(5)
              }
            }}
          >
            <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.countries}
            renderItem={({item})=>(
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({nationality:item})
                    this.RBSheet.close()
                }}
                style={{width:wp(96),paddingVertical:moderateScale(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5}}>
                    <Text style={{color:colors.darkGray, fontFamily:englishFont,fontSize:responsiveFontSize(7),marginHorizontal:moderateScale(6)}}>{item}</Text>
                </TouchableOpacity>
            )}
            />
          </RBSheet>
        )
    }


    genderSheet = () =>{
        const { isRTL } = this.props
        const { email } = this.state
        return (
            <RBSheet
            ref={ref => {
              this.genderSheetRef = ref;
            }}
            height={responsiveHeight(40)}
            duration={0}
            customStyles={{
              container: {
                //justifyContent: "center",
                alignItems: "center",
                borderTopLeftRadius:moderateScale(5),
                borderTopRightRadius:moderateScale(5)
              }
            }}
          >
            
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({gender:'femail'})
                    this.genderSheetRef.close()
                }}
                style={{width:wp(96),paddingVertical:moderateScale(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color:colors.darkGray, fontFamily:englishFont,fontSize:responsiveFontSize(7),marginHorizontal:moderateScale(6)}}>{Strings.feMale}</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={()=>{
                    this.setState({gender:'male'})
                    this.genderSheetRef.close()
                }}
                style={{width:wp(96),paddingVertical:moderateScale(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start',color:colors.darkGray, fontFamily:englishFont,fontSize:responsiveFontSize(7),marginHorizontal:moderateScale(6)}}>{Strings.male}</Text>
                </TouchableOpacity>

          </RBSheet>
        )
    }

    calender = () => {
        const {date} = this.state
        return(
        <DateTimePickerModal
        
        value={date}
        maximumDate={new Date()}
        is24Hour={true}
        isVisible={this.state.showCalender}
        mode='date'
        onConfirm={(date)=>{
            console.log("DATE   ",date)
            const d = moment(date).format('DD-MM-YYYY')
            console.log("DAT   ",d)
            this.setState({showCalender:false, birthDate:d,date:date})
        }}
        onCancel={()=>{
            this.setState({showCalender:false})
        }}
        />
       
        )
    }


    dialog=()=>{
        const { isRTL } = this.props
        return (
            <Dialog
            visible={this.state.showDialog}
            dialogAnimation={new SlideAnimation({
              slideFrom: 'bottom',
            })}
            dialogTitle={<DialogTitle title={Strings.elkhabir}/>}
            width={wp(80)}
            height={responsiveHeight(55)}
          >
            <View style={{width:wp(80),height:responsiveHeight(55)}}>
                <FastImage
                resizeMode='contain'
                source={require('../assets/imgs/appLogo.png')}
                style={{width:wp(80),height:responsiveHeight(20)}}
                />

                <Text style={{marginVertical:moderateScale(5), alignSelf:'center',width:wp(60),textAlign:'center', fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: 'black', fontFamily: boldFont }}>{Strings.mustBeVerified}</Text>
                
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showDialog:false})
                    resetTo("Login")
                    //push('MakeApplication')
                }} 
                style={{borderRadius:moderateScale(5), width:wp(40),marginTop:moderateScale(6), alignSelf:'center',height:responsiveHeight(8),backgroundColor:'#946AB3',justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(2),  color: colors.white, fontFamily: boldFont }}>{Strings.ok}</Text>
                </TouchableOpacity>

            </View>
          </Dialog>
        ) 
    }
  


    render() {
        const { loading,showCalender,nationality } = this.state
        return (
            <View style={{backgroundColor:colors.grayBackground,flex:1}}>
                    {this.backButton()}
                    <ScrollView showsVerticalScrollIndicator={false}>
                    {this.title()}
                    {this.firstNameInput()}
                    {this.lastNameInput()}
                    {this.nationnalityInput()}
                    {nationality&&this.ssnInput()}
                    {this.emailInput()}
                    {this.phoneInput()}
                    {this.birthDateInput()}
                    {this.genderInput()}
                    {this.passwordInput()}
                    {this.registerButton()}
                    {this.nationalitySheet()}
                    {this.genderSheet()}
                    {this.calender()}
                    </ScrollView>
                    {this.dialog()}
                
                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }

            

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,

    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(Register);

