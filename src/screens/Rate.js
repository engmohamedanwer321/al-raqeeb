import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import { Rating, AirbnbRating } from 'react-native-ratings';


class Rate extends Component {
    state={
      rate:'',  
      loading:false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    sendRate = () => {
        this.setState({loading:true})
        var data= new URLSearchParams()
        data.append('userId',this.props.currentUser.id)
        data.append('experience',this.state.rate)
        data.append('applicationId',this.props.data.applicationId)
        axios.post(`${BASE_END_POINT}surveys/`,data,{
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded',
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            console.log("Done")
            this.applicationStatusChannge(this.props.data.applicationId)
            
        })
        .catch(error=>{
            console.log("ERROR   ",error)
            this.setState({loading:false})
        })
    }


    applicationStatusChannge = (id) =>{
        console.log("id3   ",id)
        this.setState({loading:true })
        var data={
            status:'Completed',
            'id':id,
        }
        axios.put(`${BASE_END_POINT}applications`,data,{
            headers: {
                "Content-Type": 'application/json',
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            console.log('change application status   ',response)
            this.setState({loading:false})
            resetTo("Home")
        })
        .catch(error=>{
            this.setState({loading:false})
            console.log('error   ',error.response)
            console.log('error2   ',error)
        })
    }

    rateButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { 
                if(this.state.rate){
                    this.sendRate()
                }else{
                    RNToasty.Warn({title:Strings.pleaseRate})
                }
             }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginTop: moderateScale(20), height: responsiveHeight(8), width: wp(100), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue}} >
                <Icon name='checkcircleo' type='AntDesign' style={{color:colors.white,fontSize:responsiveFontSize(8)}} />
                <Text style={{fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(2),  color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirmAndRate}</Text>
            </TouchableOpacity>
        )
    }

    rateItem = (rateName,rateCount) => {
        const { isRTL } = this.props
        const {rate} = this.state
        return (
           <View style={{paddingHorizontal:moderateScale(5),paddingVertical:moderateScale(10), borderBottomWidth:0.5,borderBottomColor:colors.darkGray, flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'space-between', backgroundColor:'white',width:wp(100)}}>
               <TouchableOpacity 
               onPress={()=>this.setState({rate:rateCount})}
               style={{flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <View style={{width:26,height:26,borderRadius:13,backgroundColor:rate==rateCount?colors.darkBlue:'white',borderWidth:0.5,borderColor:colors.darkGray, justifyContent:'center',alignItems:'center'}} >
                        {rate==rateCount&&<Icon name='check' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(6)}} />}
                    </View>
                    <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(5), color:'black', fontFamily:englishFont}}>{rateName} </Text>
               </TouchableOpacity>
               <AirbnbRating
                starStyle={{}}
                isDisabled
                showRating={false}
                starContainerStyle={{ margin: 0 }}
                style={{ paddingVertical: 0 }}
                count={5}
                selectedColor={colors.lightBlue}
                defaultRating={rateCount}
                size={12}
                />
           </View>
        )
    }

    rateHeader = () => {
        const { isRTL } = this.props
        return (
           <View style={{justifyContent:'center',alignItems:'center', width:wp(100),backgroundColor:colors.darkBlue,paddingVertical:moderateScale(10),marginTop:moderateScale(5)}}>
                <Text style={{fontSize:responsiveFontSize(6), color:'white', fontFamily:boldFont}}>{Strings.howWasYourExp} </Text>
           </View>
        )
    }

    

  


    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader home />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={{marginTop:moderateScale(5), alignSelf:'center', fontSize:responsiveFontSize(9), color:'black', fontFamily:englishFont}}>{Strings.survey} </Text>
                    {this.rateHeader()}
                    {this.rateItem(Strings.outstanding,5)}
                    {this.rateItem(Strings.good,4)}
                    {this.rateItem(Strings.average,3)}
                    {this.rateItem(Strings.notGood,2)}
                    {this.rateItem(Strings.terrible,1)}
                    {this.rateButton()}
                   
                </ScrollView>
                {this.state.loading &&
                <LoadingDialogOverlay title={Strings.wait} />
                }
                {this.props.showMenuModal&&<Menu/>}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(Rate);

