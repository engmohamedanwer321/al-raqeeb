
export const darkBlue='#085AB7'
export const lightBlue = '#81ACDB'
export const grayBackground = '#F0F0EF'
export const darkGray = '#B3B3B3'
export const blueBackground='#075FB1'
export const grayFont = '#998FA2'


export const white = 'white';
export const black = '#373737';
export const sky = '#55B1C2'

export const lightGray = '#f2f2f0'
export const green = 'green'
export const greenApp = '#80ad30'
export const grayButton = '#6b6b6b'

export const grayColor1 ='#737373'

export const lightGreen = '#31AA48'
export const medimGreen = '#249843'
export const darkGreen = '#0B763B'
export const greenButton='#1e8b06'
export const darkRed ='#df0000'

