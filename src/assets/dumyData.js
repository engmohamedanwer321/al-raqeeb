export const countries = {
    AM: "Armenia",
    AL: "Albania",
    AO: "Angola",
    AQ: "Antarctica",
    AS: "American Samoa",
    AR: "Argentina",
    AU: "Australia",
    AT: "Austria",
    AW: "Aruba",
    AD: "Andorra",
    AG: "Antigua and Barbuda",
    AF: "Afghanistan",
    AI: "Anguilla",
    AX: "Aland Islands",
    AZ: "Azerbaijan",
    DZ: "Algeria",
    
    BD: "Bangladesh",
    BE: "Belgium",
    BF: "Burkina Faso",
    BG: "Bulgaria",
    BA: "Bosnia and Herzegovina",
    BB: "Barbados",
    BM: "Bermuda",
    BN: "Brunei",
    BO: "Bolivia",
    BH: "Bahrain",
    BI: "Burundi",
    BJ: "Benin",
    BT: "Bhutan",
    BW: "Botswana",
    BQ: "Bonaire, Saint Eustatius and Saba ",
    BR: "Brazil",
    BS: "Bahamas",
    BV: "Bouvet Island",
    BY: "Belarus",
    BZ: "Belize",
    IO: "British Indian Ocean Territory",
    VG: "British Virgin Islands",

    CK: "Cook Islands",
    CO: "Colombia",
    CN: "China",
    CM: "Cameroon",
    CL: "Chile",
    CC: "Cocos Islands",
    CA: "Canada",
    CZ: "Czech Republic",
    CY: "Cyprus",
    CX: "Christmas Island",
    CR: "Costa Rica",
    CW: "Curacao",
    CV: "Cape Verde",
    CU: "Cuba",
    CF: "Central African Republic",
    KY: "Cayman Islands",
    KH: "Cambodia",
    TD: "Chad",
    HR: "Croatia",


    CD: "Democratic Republic of the Congo",
    DO: "Dominican Republic",
    DM: "Dominica",
    DJ: "Djibouti",
    DK: "Denmark",


    TL: "East Timor",
    GQ: "Equatorial Guinea",
    SV: "El Salvador",
    EE: "Estonia",
    EG: "Egypt",
    ET: "Ethiopia",


    FR: "France",
    FI: "Finland",
    FJ: "Fiji",
    FK: "Falkland Islands",
    GF: "French Guiana",
    PF: "French Polynesia",


    GW: "Guinea-Bissau",
    GU: "Guam",
    GT: "Guatemala",
    GR: "Greece",
    GP: "Guadeloupe",
    GY: "Guyana",
    GG: "Guernsey",
    GE: "Georgia",
    GD: "Grenada",
    GA: "Gabon",
    GN: "Guinea",
    GM: "Gambia",
    GL: "Greenland",
    GI: "Gibraltar",
    GH: "Ghana",


    HT: "Haiti",
    HU: "Hungary",
    HK: "Hong Kong",
    HN: "Honduras",
    HM: "Heard Island and McDonald Islands",

    IQ: "Iraq",
    IN: "India",
    IS: "Iceland",
    IR: "Iran",
    IE: "Ireland",
    ID: "Indonesia",
    IT: "Italy",

    LI: "Liechtenstein",
    LV: "Latvia", 
    LT: "Lithuania",
    LU: "Luxembourg",
    LR: "Liberia",
    LS: "Lesotho",
    LY: "Libya",


    
    ME: "Montenegro",
    MD: "Moldova",
    MG: "Madagascar",
    MF: "Saint Martin",
    MA: "Morocco",
    MC: "Monaco",
    MM: "Myanmar",
    ML: "Mali",
    MO: "Macao",
    MN: "Mongolia",
    MH: "Marshall Islands",
    MK: "Macedonia",
    MU: "Mauritius",
    MT: "Malta",
    MW: "Malawi",
    MV: "Maldives",
    MQ: "Martinique",
    MS: "Montserrat",
    MR: "Mauritania",
    MY: "Malaysia",
    MX: "Mexico",
    FM: "Micronesia",
    MZ: "Mozambique",


    MP: "Northern Mariana Islands",
    NI: "Nicaragua",
    NL: "Netherlands",
    NO: "Norway",
    NA: "Namibia",
    NC: "New Caledonia",
    NE: "Niger",
    NF: "Norfolk Island",
    NG: "Nigeria",
    NZ: "New Zealand",
    NP: "Nepal",
    NR: "Nauru",
    NU: "Niue",
    KP: "North Korea",
 


    PR: "Puerto Rico",
    PS: "Palestinian Territory",
    PW: "Palau",
    PT: "Portugal",
    PY: "Paraguay",  
    PA: "Panama",  
    PG: "Papua New Guinea",
    PE: "Peru",
    PK: "Pakistan",
    PH: "Philippines",
    PN: "Pitcairn",
    PL: "Poland",

    


    BL: "Saint Barthelemy",    
    WS: "Samoa",
    GS: "South Georgia and the South Sandwich Islands",
    RS: "Serbia",
    SJ: "Svalbard and Jan Mayen",
    SZ: "Swaziland",
    SY: "Syria",
    SX: "Sint Maarten",
    

    JM: "Jamaica",
    JE: "Jersey",
    JO: "Jordan",
    JP: "Japan",
 
    RU: "Russia",
    RO: "Romania",
    RW: "Rwanda",
    RE: "Reunion",



   
    
    TM: "Turkmenistan",
    TJ: "Tajikistan",
    TK: "Tokelau",
    TN: "Tunisia",
  
  

   



    GB: "United Kingdom",
   
    OM: "Oman",
  
    
    VE: "Venezuela",
    
   
    PM: "Saint Pierre and Miquelon",
    ZM: "Zambia",
    EH: "Western Sahara",
  
    ZA: "South Africa",
    EC: "Ecuador",
    
    VN: "Vietnam",
    SB: "Solomon Islands",
    
    SO: "Somalia",
    ZW: "Zimbabwe",
    SA: "Saudi Arabia",
    ES: "Spain",
    ER: "Eritrea",

    UZ: "Uzbekistan",

    IM: "Isle of Man",
    UG: "Uganda",
    TZ: "Tanzania",
  
 
    SH: "Saint Helena",
    


    FO: "Faroe Islands",
  
   
    VU: "Vanuatu",
    XK: "Kosovo",
    CI: "Ivory Coast",
    CH: "Switzerland",

   

    CG: "Republic of the Congo",
   

   

  
   
    KG: "Kyrgyzstan",
    KE: "Kenya",
    SS: "South Sudan",
    SR: "Suriname",
    KI: "Kiribati",
    KN: "Saint Kitts and Nevis",
    KM: "Comoros",
    ST: "Sao Tome and Principe",
    SK: "Slovakia",
    KR: "South Korea",
    SI: "Slovenia",
   
    SN: "Senegal",
    SM: "San Marino",
    SL: "Sierra Leone",
    SC: "Seychelles",
    KZ: "Kazakhstan",

    SG: "Singapore",
    SE: "Sweden",
    SD: "Sudan",

    DE: "Germany",
    YE: "Yemen",
    
    US: "United States",
    UY: "Uruguay",
    YT: "Mayotte",
    UM: "United States Minor Outlying Islands",
    LB: "Lebanon",
    LC: "Saint Lucia",
    LA: "Laos",
    TV: "Tuvalu",
    TO: "Tonga",
    TW: "Taiwan",
    TT: "Trinidad and Tobago",
    TR: "Turkey",
    LK: "Sri Lanka",
   
  

    TH: "Thailand",
    TF: "French Southern Territories",
    TG: "Togo",
    
    TC: "Turks and Caicos Islands",

    VA: "Vatican",
    VC: "Saint Vincent and the Grenadines",
    AE: "United Arab Emirates",
   
    VI: "U.S. Virgin Islands",
    
    

    UA: "Ukraine",
    QA: "Qatar",
    KW: "Kuwait",
 
}